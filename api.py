from flask import Flask, jsonify, request
DB_PATH = 'database.db'  # Path to SQLite database file
import sqlite3
app = Flask(__name__)

# @app.route('/api', methods=['POST'])
# def api_handler():
#     data = request.json()
#     return jsonify(data)

@app.route('/api', methods=['GET'])
def api_handler():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    # Retrieve the message from the database
    cursor.execute("SELECT message FROM messages WHERE id = 1")
    result = cursor.fetchone()
    message = {'message': result[0]} if result else {'message': 'No message found'}

    conn.close()

    return jsonify(message)


@app.route('/api', methods=['POST'])
def update_message():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    # Retrieve the new message from the request body
    new_message = request.json.get('message')

    # Update the message in the database
    cursor.execute("UPDATE messages SET message = ? WHERE id = 1", (new_message,))
    conn.commit()

    conn.close()

    return jsonify({'message': 'Message updated successfully'})


@app.route('/api', methods=['GET'])
def api_handler():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    # Retrieve the message from the database
    cursor.execute("SELECT message FROM messages WHERE id = 1")
    result = cursor.fetchone()
    message = {'message': result[0]} if result else {'message': 'No message found'}

    conn.close()

    return jsonify(message)

@app.route('/api', methods=['POST'])
def update_message():

   
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    # Retrieve the new message from the request body
    new_message = request.json.get('message')

    # Update the message in the database
    cursor.execute("UPDATE messages SET message = ? WHERE id = 1", (new_message,))
    conn.commit()

    conn.close()

    return jsonify({'message': 'Message updated successfully'})



if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
