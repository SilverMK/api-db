# Use a base image with the required runtime environment
FROM python:3

# Install the 'cryptography' package
RUN pip install cryptography


# Set the working directory inside the container
WORKDIR /API

# Copy the API code to the working directory
COPY . /API

RUN pwd

# Install any necessary dependencies
RUN pip install -r requirements.txt

# Expose the port on which your API will run
# EXPOSE 5000

# Set the command to run your API
CMD ["python", "db.py"]

